#!/usr/bin/env python3

from flask import Flask, request, redirect, make_response
app = Flask(__name__)

import planck

@app.route('/')
def index():
    return redirect(request.url_root + 'time/now')

@app.route('/time/now')
def time_now():
    plancks_since_universe = str(planck.time.now())
    return make_response(plancks_since_universe, 200)

if __name__ == '__main__':
    app.run(port=80)
